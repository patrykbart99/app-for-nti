<?php

require ("vendor/autoload.php");

session_start();

$fb = new \Facebook\Facebook([
    'app_id' => '2197575180555030',
    'app_secret' => 'db7f2bb7c49d9e64f5217e25db2070d4',
    'default_graph_version' => 'v3.3',
]);

$helper = $fb->getRedirectLoginHelper();
$login_url = $helper->getLoginUrl("http://localhost:8888/test/facebook_login.php");

try {
    $accessToken = $helper->getAccessToken();
    if(isset($accessToken)) {
        $_SESSION['access_token'] = (string)$accessToken;

        //redirect
        header("Location:index.php");
    }
}

catch(Exception $exc) {
    echo $exc->getTraceaAsString();
}

//getting users first name, last name, e-mail
if($_SESSION['access_token']) {
    try {
        $fb->setDefaultAccessToken($_SESSION['access_token']);
        $res =  $fb->get('/me?locale=en_US&fields=name,email', $accessToken);
        $user = $res->getGraphuser();
        echo $user['name'].'<br/>';
        echo $user['enail']; 
    }
    catch(Exception $exc) {
        echo $exc->getTraceAsString();
    }
}
?>