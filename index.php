<?php require 'facebook_login.php'; ?>
<html>
    <head>
        <title>Facebook LogIn</title>
        <meta charset = "utf-8">
        <link rel = "stylesheet" href = "main.css">
    </head>

    <body>
        <img src = "img/fb_logo.png">
        <?php if(isset($_SESSION['access_token'])): ?>
        <br/><a href = "logout.php"><button>Logout</button></a>
        <?php
            else:
        ?>
        <a href = "<?php echo $login_url;?>"><button>Log In with Facebook</button></a>
        <?php
            endif;
        ?>
    </body>
</html>